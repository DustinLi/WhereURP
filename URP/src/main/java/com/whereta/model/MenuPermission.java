package com.whereta.model;

import java.io.Serializable;

public class MenuPermission implements Serializable {
    /**
     * menu_permission.id
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer id;

    /**
     * menu_permission.menu_id (菜单id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer menuId;

    /**
     * menu_permission.permission_id (权限id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer permissionId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
}