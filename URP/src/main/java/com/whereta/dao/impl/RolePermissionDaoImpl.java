package com.whereta.dao.impl;

import com.whereta.dao.IRolePermissionDao;
import com.whereta.mapper.RolePermissionMapper;
import com.whereta.model.RolePermission;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @author Vincent
 * @time 2015/8/27 17:26
 */
@Repository("rolePermissionDao")
public class RolePermissionDaoImpl implements IRolePermissionDao {
    @Resource
    private RolePermissionMapper rolePermissionMapper;

    /**
     * 根据角色id获取权限id集合
     * @param roleId
     * @return
     */
    public Set<Integer> getPermissionIdSetByRoleId(int roleId) {
        return rolePermissionMapper.getPermissionIdSetByRoleId(roleId);
    }

    /**
     * 根据权限删除role-per
     * @param perId
     */
    public void deleteByPerId(int perId) {
            rolePermissionMapper.delete(null,perId);
    }

    /**
     * 根据角色删除role-per
     * @param roleId
     */
    public void deleteByRoleId(int roleId) {
        rolePermissionMapper.delete(roleId,null);
    }

    /**
     * 添加角色权限
     * @param rolePermission
     * @return
     */
    public int addRolePermission(RolePermission rolePermission) {
        return rolePermissionMapper.insertSelective(rolePermission);
    }
}
