package com.whereta.dao;

import com.whereta.model.QueryAccountPermission;

/**
 * Created by vincent on 15-9-5.
 */
public interface IQueryAccountPermissionDao {

    QueryAccountPermission querySql(int id);

}
